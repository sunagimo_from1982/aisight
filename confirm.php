<?php
$company_name = $_POST['company_name'];
$company_department = $_POST['company_department'];
$name = $_POST['name'];
$email = $_POST['email'];
$tel = $_POST['tel'];
// if (isset($_POST['inquiry']) && is_array($_POST['inquiry'])) {
//   $inquiry = implode('<br>', $_POST['inquiry']);
// }
$detail = $_POST['detail'];
$inquiry = $_POST['inquiry'];
$agree = $_POST['agree'];

// $filedown = $_POST['filedown'];
?><!doctype html>
<html>
<head>

<meta charset="utf-8">
<title>AIでSNS上のUGCを瞬時に分析するツール｜SOCIAL PROFILING(ソーシャルプロファイリング)</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KH6NDPF');</script>
<!-- End Google Tag Manager -->
	
<meta name="description" content="プロファイリングAIで、細かなインサイトを掴む。SNS上の膨大な量のUGCを瞬時に分析。一般的なソーシャルの調査では紐付けられない“流行りの投稿“とユーザーの“属性情報“を可視化。AIQ株式会社のSOCIAL PROFILING（ソーシャルプロファイリング）。">
<meta name="keywords" content="UGC,分析,調査,SNS,SOCIAL,PROFILING">

<meta property="og:type" content="article">
<meta property="og:title" content="AIでSNS上のUGCを瞬時に分析するツール｜SOCIAL PROFILING(ソーシャルプロファイリング)">
<meta property="og:image" content="./asset/img/ogp.png">
<meta property="og:url" content="https://aiqlab.com/social_profiling">
<meta property="og:description" content="プロファイリングAIで、細かなインサイトを掴む。SNS上の膨大な量のUGCを瞬時に分析。一般的なソーシャルの調査では紐付けられない“流行りの投稿“とユーザーの“属性情報“を可視化。AIQ株式会社のSOCIAL PROFILING（ソーシャルプロファイリング）。">
<meta property="og:site_name" content="AIQ株式会社">

<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- favicon -->
<link rel="icon" href="./asset/img/favicon.ico">	
	
<link rel="apple-touch-icon" href="">
<link rel="shortcut icon" href="">
	
	
<link rel="stylesheet" href="./asset/css/fonts.css"  type="text/css">	
<link rel="stylesheet" href="./asset/css/reset.css"  type="text/css">	
<link rel="stylesheet" href="./asset/css/style.css"  type="text/css">	
<link rel="stylesheet" href="./asset/css/style_sp.css"  type="text/css">	
<link rel="stylesheet" href="./asset/css/slick.css"  type="text/css">	
<link rel="stylesheet" href="./asset/css/slick-theme.css"  type="text/css">	

<script src="./asset/js/jquery-3.1.1.min.js"></script>	
<script src="./asset/js/common.js"></script>	
<script src="./asset/js/slick.min.js"></script>	
	
</head>

<body>
	<div class="container">
		<header>
			<div class="header-inner">
				<div class="header-title">
					<a href="./">
						<img class="" src="./asset/img/header_logo.png" alt="SOCIAL PROFILING">
					</a>
				</div>
				<nav>
					<div id="nav__btn" class="sp"><i></i></div>
					<img class="header-navlogo sp" src="./asset/img/header_logo.png" alt="SOCIAL PROFILING">

					<ul>
						<li>
							<a href="./#title1">
								選ばれる理由
							</a>
						</li>
						<li>
							<a href="./#title2">
								分析方法
							</a>
						</li>
						<li>
							<a href="./#title3">
								プロファイリングAIのしくみ
							</a>
						</li>
						<li>
							<a href="./#title4">
								活用例
							</a>
						</li>
					</ul>
					<div class="header-dlbtn">
						<a href="./#title5" target="_blank">
							<img class="pc" src="./asset/img/header-btn.png" alt="無料資料ダウンロード">
							<img class="sp" src="./asset/img/sp/main001_btn1.png" alt="無料資料ダウンロード">
						</a>
					</div>
				</nav>
			</div>
		</header>
		<div class="spacer"></div>
		<main>
			<div class="main-inner">

				
				<section class="main-010">
					<h2 class="">カタログ無料ダウンロード<br class="sp">申し込みフォーム</h2>
					<h3 class="">SOCIAL PROFILING<br class="sp">（ソーシャルプロファイリング）の<br class="sp">活用例が掲載された<br>カタログを無料でダウンロード<span class="kerning">。</span></h3>
				
					<p class=""><span class="red">*</span>入力必須事項</p>
					<form method="post" action="download.html" id="form">
					<dl class="">
						<dt>企業名<span class="red">*</span></dt>
						<dd>
							<input type="text" id="company_name" name="company_name" value="<?=$company_name?>" readonly>
						</dd>
						<dt>部署名<span class="red">*</span></dt>
						<dd>
							<input type="text" id="company_department" name="company_department" value="<?=$company_department?>" readonly>
						</dd>
						<dt>お名前<span class="red">*</span></dt>
						<dd>
							<input type="text" id="name" name="name" value="<?=$name?>" readonly>
						</dd>
						<dt>電話番号<span class="red">*</span></dt>
						<dd>
							<input type="text" id="tel" name="tel" value="<?=$tel?>" readonly>
						</dd>
						<dt>メールアドレス<span class="red">*</span></dt>
						<dd>
							<input type="text" id="email" name="email" value="<?=$email?>" readonly>
						</dd>
						<dt>お問い合わせ内容</dt>
						<dd>
				            <?php if($inquiry == "1"): ?>
							<label><input type="radio" class="radio1" name="inquiry_sub" id="inquiry1" value="サービスの概要について知りたい" checked disabled><span class="radio1span" ></span>サービスの概要について知りたい</label>
							
							<?php else: ?>
							<label><input type="radio" class="radio1" name="inquiry_sub" id="inquiry1" value="サービスの概要について知りたい" disabled><span class="radio1span" ></span>サービスの概要について知りたい</label>
							<?php endif ?>
							
				            <?php if($inquiry == "2"): ?>
							<label><input type="radio" class="radio1" name="inquiry_sub" id="inquiry2" value="導入期間、見積もり、機能を知りたい" checked disabled><span class="radio1span"></span>導入期間<span class="kerning">、</span>見積もり<span class="kerning">、</span>機能を知りたい</label>
							<?php else: ?>
							<label><input type="radio" class="radio1" name="inquiry_sub" id="inquiry2" value="導入期間、見積もり、機能を知りたい" disabled><span class="radio1span"></span>導入期間<span class="kerning">、</span>見積もり<span class="kerning">、</span>機能を知りたい</label>
							<?php endif ?>
							
				            <?php if($inquiry == "3"): ?>
							<label><input type="radio" class="radio1" name="inquiry_sub" id="inquiry3" value="クライアントに提案希望" checked disabled><span class="radio1span"></span>クライアントに提案希望</label>
							<?php else: ?>
							<label><input type="radio" class="radio1" name="inquiry_sub" id="inquiry3" value="クライアントに提案希望" disabled><span class="radio1span"></span>クライアントに提案希望</label>
							<?php endif ?>
							
				            <?php if($inquiry == "4"): ?>
							<label><input type="radio" class="radio1" name="inquiry_sub" id="inquiry4" value="自社のサービスと連携したい業務提携希望" checked disabled><span class="radio1span"></span>自社のサービスと連携したい<span class="kerning">、</span>業務提携希望</label>
							<?php else: ?>
							<label><input type="radio" class="radio1" name="inquiry_sub" id="inquiry4" value="自社のサービスと連携したい業務提携希望" disabled><span class="radio1span"></span>自社のサービスと連携したい<span class="kerning">、</span>業務提携希望</label>
							<?php endif ?>
				            <?php if($inquiry == "5"): ?>
							<label><input type="radio" class="radio1" name="inquiry_sub" id="inquiry5" value="その他" checked disabled><span class="radio1span"></span>その他</label>
							<?php else: ?>
							<label><input type="radio" class="radio1" name="inquiry_sub" id="inquiry5" value="その他" disabled><span class="radio1span"></span>その他</label>
							<?php endif ?>
							<input type="hidden" id="inquiry" name="inquiry" value="<?=$inquiry?>" readonly>
						</dd>
						<dd>
							<textarea id="detail" name="detail" readonly><?=$detail?></textarea>
						</dd>
						<dt>個人情報のお取り扱いについての同意<span class="red">*</span></dt>
						<dd>
							
							<label><input type="checkbox" class="cb1" id="agree" value="1" name="agree" checked disabled ><span class="cb1span"></span>お問合せに関する<a href="https://aiqlab.com/policy/" target="_blank">個人情報のお取り扱いについて</a>同意します。</label>
						</dd>
					</dl>
					<a class="main-010__btn2  " href="javascript:void(0);" onClick="submitData();">
						<img class="pc" src="./asset/img/confirm.png" alt="内容を送信してカタログをダウンロードする">
						<img class="sp" src="./asset/img/sp/confirm.png" alt="内容を送信してカタログをダウンロードする">
					</a>
					<a class="main-010__btn3 " href="javascript:void(0);" onClick="history.back();">
						<img src="./asset/img/back.png" alt="戻る">
					</a>
					</form>
				</section>
			</div>
			
		</main>
		<footer>
			<div class="footer-inner">
				<div class="footer-top">
					<a href="https://aiqlab.com/policy/" target="_blank">プライバシーポリシー</a>
					<a href="https://aiqlab.com/about2/" target="_blank">運営会社</a>
				</div>
				<p>Copyright &copy; AIQ corporation All Rights Reserved.</p>
			</div>
		</footer>
	</div>
<script>
	$(document).ready(function() {

		
	});
	
	
	function submitData(){

		// 状態確認
		if (checkSubmit() == false) return false;

		inquiry_data = "";
		
		$("input[name='inquiry_sub']:checked").each(function(){
			inquiry_data = $(this).val();
		});

		$.post('https://ailink.aiqlab.com/aiq/social_profiling_lp_contact',
		{
			type : 'default',
			company : $('#company_name').val(),
			department : $('#company_department').val(),
			name : $('#name').val(),
			tel : $('#tel').val(),
			email : $('#email').val(),
			inquiry : inquiry_data,
			detail : $('#detail').val(),
			// download : $('#filedown').prop('checked'),
		}
		).done(function(data, textStatus, jqXHR){

		if( data.result == 'OK' ){
			submit_flg = false;
			location.href = 'download.html';
		} else {
			if( data.message == undefined ){
			  alert('エラーが発生しました。');
			} else {
			  alert( data.message );
			}
			submit_flg = false;
			}
		}).fail(function(jqXHR, textStatus, errorThrown){
			alert('エラーが発生しました。');
			submit_flg = false;
		});


		return false;
	}
	
	function checkSubmit(){
		if( $('#company_name').val() == '' )   return false;
		if( $('#company_department').val() == '' )   return false;
		if( $('#name').val() == '' )   return false;
		if( $('#tel').val() == '' )  return false;
		if( $('#email').val() == '' )  return false;
		// if( $('#detail').val() == '' ) return false;
		if( $('#agree').prop('checked') == false ) return false;
		// if($('#filedown').prop('checked') == false ){
		//   if( $('#detail').val() == '' ) return false;
		// }

		return true;
  }
	
	function backPage(){
		history.back();
		return false;
	}
</script>

</body>
</html>
