// JavaScript Document

$(function(){
	$('#nav__btn').on('click',function(){
		$(this).parents('nav').toggleClass('open');


	});
	
	$('[href^="#"]').on('click', function() {
		$(this).parents('nav').removeClass('open');
		let speed = 800;
		let href= $(this).attr("href");
		let target = $(href == "#" || href == "" ? 'html' : href);
		let position = target.offset().top - 71;
		$('body,html').animate({scrollTop:position}, speed, 'swing');
		return false;
	});
	
	
	
});

var chk1 = false;
var chk2 = false;
var chk3 = false;
var chk4 = false;
var chk5 = false;
var chk6 = false;
var chk7 = false;
var chk8 = false;
var chk9 = false;
var chk10 = false;

function fadeinItem(){
	chk1 = fadeItems(".op1",4,chk1);
	chk2 = fadeItems(".op2",4,chk2);
	chk3 = fadeItems(".op3",9,chk3);
	chk4 = fadeItems(".op4",2,chk4);
	chk5 = fadeItems(".op5",8,chk5);
	chk6 = fadeItems(".op6",3,chk6);
	chk7 = fadeItems(".op7",6,chk7);
	chk8 = fadeItems(".op8",5,chk8);
	chk9 = fadeItems(".op9",6,chk9);
	chk10 = fadeItems(".op10",4,chk10);
	/*chk11 = fadeItems("#op11",chk11);
	chk12 = fadeItems("#op12",chk12);
	chk13 = fadeItems("#op13",chk13);*/

    // windowがスクロールされた時に実行する処理
}

function fadeItems(ids,itemCnt,chk){
	var minus = 500;
	var p =$(window).scrollTop();
	var w = $(window).width();
	
	/*if(ids == "#op2"){
		minus = 1600;
		if(w >= 1500){
			minus += 500;
		}
	}else if(ids == "#op3"){
		minus = 1200;
	}else if(ids == "#op10"){
		minus = 1000;
	}*/
	
	
	
	
	var x = 768;
	if (w < x) {
		minus -= 200;
	}else if(w >= 1500){
		minus += 400;
	}

	if($(ids).offset().top - minus < p){
		if(!chk){
			
			chk = true;
			for(var i = 1; i <= itemCnt;i++){
				if(!$(ids + "-" + i).hasClass('opacity100')){
					if(ids == ".op1" && i >= 2){
						if(i == 2){
							setTimeoutFunc(ids,i,i * 500);
						}else if(i == 3){
							setAnimeFunc(ids,i,i * 800);
						}else if(i == 4){
							setLoadFunc(ids,i,i * 800);
						}
					}else{
						setTimeoutFunc(ids,i,i * 300);
					}
	
				}
			}
		}
	}
		

	return chk;
}
function setTimeoutFunc(ids,i,timeout){
	setTimeout(function(){
		$(ids + "-" + i).addClass("opacity100");
	},timeout);
}
function setAnimeFunc(ids,i,timeout){
	setTimeout(function(){
		$(ids + "-" + i).addClass("loading");
	},timeout);
}
function setLoadFunc(ids,i,timeout){
	setTimeout(function(){
		$(ids + "-" + i).addClass("img_anime");
	},timeout);
}

